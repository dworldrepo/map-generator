# -*- coding: utf-8 -*-
"""
Created on Sat Jul 28 19:25:22 2018

@author:
"""
from PIL import Image
from noise import snoise2
import math
import random
from datetime import datetime
from pathlib import Path
from copy import deepcopy
import time

# All the big math modules, special space because they're a bit confusing

import matplotlib.pyplot as plt
import numpy as np

from matplotlib import colors
from matplotlib.ticker import PercentFormatter

from functions import *


class Map():
    """Forms the basis of all map classes"""

    def __init__(self, width, height, seed_num, rand_seed_num):

        self.rand_seed_num = rand_seed_num  # Random seed used to replicate results
        random.seed(self.rand_seed_num)  # Set the random seed value so we can replicate results

        self.width = width  # Width of map
        self.height = height  # Height of map

        self.seed_num = seed_num  # Number of land seeds used to grow land masses.

        self.seed_proximity = 0.3  # Determined how close seeds may be placed to each other, scaled to map dimensions.
        self.height_normal = 0.5  # This is added to the Perlin Noise value generated, 0.5 by default
        self.parent_proximity = 0.5  # Lifts land around land seeds, VERY SENSITIVE. Larger means larger lift

        # Settings that determine what kind of terrain a point is.
        self.land_threshold = 0.5  # Determines if point is over or under see level.
        self.mount_threshold = 0.8  # If over this, the point is mountainous
        self.low_water_threshold = 0.48  # If water point over this, point is low water

        # Settings affecting perlin noise generation and also influencing the final height value
        self.base_perlin_offset = random.random() * 2048
        self.perlin_scale = 0.0025
        self.map_detail = 3  # Number of octaves used by perlin noise
        self.persistence = 0.5
        self.lacunarity = 2.0

        self.raise_land = -0.15  # Raise land by a fixed amount, can be negative

        # These variables store important data structures
        self.points = []
        self.seed_coords = []
        self.height_map = [[0 for x in range(self.width)] for y in range(self.height)]
        self.color_map = [[(0, 0, 0) for x in range(self.width)] for y in range(self.height)]
        #self.flood_fill_map = [[None for x in range(self.width)] for y in range(self.height)]
        self.region_map = [[0 for x in range(self.width)] for y in range(self.height)]

        self.height_image = None  # Save the image of the height map here
        self.debug_height_image = None  # Debug image, used to locate things like seeds
        self.region_image = None  # Region map, lots of different colors!

        # "Region 1" : ["Name", "Size", "Type"]
        self.region_info = {}

        # Now we create the map with all its structures

    def create_map(self):

        start = time.time()
        self.seed_coords = self.place_seeds()
        end = time.time()
        print("Seeds complete: {} seconds".format((end - start)))

        start = time.time()
        self.create_height_map()
        end = time.time()
        print("Height map calculated: {} seconds".format((end - start)))

        start = time.time()
        self.create_color_map()
        end = time.time()
        print("Color map calculated: {} seconds".format((end - start)))

        start = time.time()
        self.draw_height_map(True)  # If true, debug setting is enabled
        end = time.time()
        print("Height map drawn: {} seconds".format((end - start)))


        start = time.time()
        self.flood_regions()
        end = time.time()
        print("Regions created: {} seconds".format((end - start)))


        start = time.time()
        self.draw_region_map()
        end = time.time()
        print("Region map drawn: {} seconds".format((end - start)))


        start = time.time()
        self.save_images(False)  # Use true if you also want to save the data on the image
        end = time.time()
        print("Images saved: {} seconds".format((end - start)))


    def place_seeds(self):
        """ Function that places the region/island seeds around the map randomly,
        while taking into consideration the minimum distance given by seed_proximity_min
        """
        seed_coords = []
        # Takes number of seeds and points to spread them across as arguments
        for seed in range(self.seed_num):
            loop_limit = 5000  # Prevents an infinite loop from happening. This limit is actually reached very quickly
            if seed == 0:
                """The -2 is there because of the visual representations of the seeds, which is bigger than 1 pixel. 
                Otherwise you get errors when seed_coord is @(x, 1) or (1, y). Same reason rand starts at 1.
                """
                seed_coords.append([random.randint(0, self.width), random.randint(0, self.height)])
            else:
                other_seeds = seed_coords[:]
                seed_coords.append([random.randint(0, self.width), random.randint(0, self.height)])

                all_seeds_done = False
                while not all_seeds_done:
                    seeds_to_do = len(other_seeds)
                    #                print(other_seeds, "*"*20)
                    for otherseed in other_seeds:
                        if norm_dist(otherseed[0], otherseed[1], seed_coords[seed][0], seed_coords[seed][1],
                                     self.height,
                                     self.width) <= self.seed_proximity:
                            seed_coords[seed] = [random.randint(0, self.width), random.randint(0, self.height)]
                        #                        print(seed_coords)
                        else:
                            seeds_to_do -= 1

                        if seeds_to_do == 0:  # All seeds have been verified
                            all_seeds_done = True

                        loop_limit -= 1
                        # No possible locations to place seed, so last position is used and loop is broken.
                        if loop_limit == 0:
                            all_seeds_done = True
                            print("Seed could not not find appropriate position")

        # Now that we have a bunch of random seed_coords, we assign them one of the
        # points on the actual map

        new_seed_coords = []

        for seed in seed_coords:
            nearest_point = 0.0
            distance = 0.0
            for point in seed_coords[:]:
                if nearest_point == 0.0:
                    nearest_point = point
                    distance = calc_dist(point[0], point[1], seed[0], seed[1])
                elif calc_dist(point[0], point[1], seed[0], seed[1]) < distance:
                    nearest_point = point
                    distance = calc_dist(point[0], point[1], seed[0], seed[1])
            new_seed_coords.append(nearest_point)

        return new_seed_coords

    def create_height_map(self):
        """This function will populate the height map with values, using perlin noise and the seed coordinates"""

        for x in range(0, self.width):
            for y in range(0, self.height):
                self.height_map[y][x] = self.calc_point_height(x, y)

    def calc_point_height(self, x_coord, y_coord):
        """ Calculates what the height of the input point is."""

        # First we determine the parent seed
        # parentSeed = []
        dist_to_parent = self.width * self.height  # This is just an absurdly high number for reference. Note that this
        #  value is normalized
        for seed in self.seed_coords:
            if norm_dist(seed[0], seed[1], x_coord, y_coord, self.width, self.height) < dist_to_parent:
                # parentSeed = seed
                dist_to_parent = norm_dist(seed[0], seed[1], x_coord, y_coord, self.width, self.height)

        # Now we use Perlin Noise to generate the actual pixel
        base_perlin_value = (snoise2(float(x_coord) * self.perlin_scale, float(y_coord) * self.perlin_scale,
                                     octaves=self.map_detail, persistence=self.persistence, lacunarity=self.lacunarity,
                                     repeatx=2048, repeaty=2048, base=self.base_perlin_offset))

        # base_perlin_value -= dist_to_parent**(self.parent_proximity)

        # Modifiers to the height output, allowing for more refined tweaking
        new_height_value = base_perlin_value + dist_to_parent ** (1 / self.parent_proximity)

        new_height_value += self.height_normal + self.raise_land

        if new_height_value <= 0:
            new_height_value = 0
        elif new_height_value > 1:
            new_height_value = 1

        return new_height_value

    def create_color_map(self):

        for x in range(0, self.width):
            for y in range(0, self.height):
                if self.height_map[y][x] > self.mount_threshold:  # Tile is Mountain
                    self.color_map[y][x] = (128, 128, 128)
                elif self.height_map[y][x] > self.land_threshold:  # Tile is Land
                    self.color_map[y][x] = (153, 102, 51)
                elif self.height_map[y][x] > self.low_water_threshold:  # Tile is Low Water
                    self.color_map[y][x] = (0, 204, 255)
                else:  # Tile is Water
                    self.color_map[y][x] = (0, 0, 255)

    def draw_height_map(self, debug=False):

        # Height map generator
        self.height_image = Image.new("RGB", (self.width, self.height))
        for x in range(0, self.width):
            for y in range(0, self.height):
                self.height_image.putpixel((x, y), self.color_map[y][x])

        if debug:
            self.debug_height_image = deepcopy(self.height_image)
            for seed in self.seed_coords:
                self.debug_height_image.putpixel((seed[0] - 1, seed[1] - 1), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0], seed[1] - 1), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0] + 1, seed[1] - 1), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0] - 1, seed[1]), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0], seed[1]), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0] + 1, seed[1]), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0] - 1, seed[1] + 1), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0], seed[1] + 1), (255, 0, 0))
                self.debug_height_image.putpixel((seed[0] + 1, seed[1] - 1), (255, 0, 0))

        # plt.imshow(np.asarray(self.height_image))

    def save_images(self, save_data=False):

        # First we check if the image name has already been used, if yes we increase the number.
        image_num = 1  # Always start checking at 1
        height_image_path = Path("images/map{}.png".format(image_num))

        while height_image_path.is_file():
            # print("Am I in a loop?")
            image_num += 1
            height_image_path = Path("images/map{}.png".format(image_num))

        else:  # Save the file
            #print("Saving image" + str(image_num))
            self.height_image.save("images/map{}.png".format(image_num))
            self.region_image.save("images/rmap{}.png".format(image_num))

        if save_data:
            with open("data/map{}.txt".format(image_num), "w", newline="") as txtfile:
                txtfile.write("Map Dimensions (x,y): " + str(self.width) + " , " + str(self.height))
                txtfile.write("Random Seed: " + str(self.rand_seed_num))

    # def flood_tile(self, x, y, region_num):
    #
    #     stack = [(x, y)]
    #     orig_fill_type = None  # Remember what type the original point was, i.e. land or water
    #
    #     if self.height_map[y][x] > self.land_threshold:
    #         orig_fill_type = 1
    #     else:
    #         orig_fill_type = 0
    #
    #     while len(stack) > 0:
    #
    #         x, y = stack.pop()
    #
    #         if self.region_map[y][x] != 0:
    #             # Ignore points that have already been calculated, they are part of different region
    #             continue
    #
    #         if self.height_map[y][x] > self.land_threshold:  # Tile is Land
    #             self.flood_fill_map[y][x] = 1
    #         else:
    #             self.flood_fill_map[y][x] = 0
    #
    #         # There might be problems in this logic when it comes to considering points that are at the edge of regions
    #         if self.flood_fill_map[y][x] == orig_fill_type:
    #             self.region_map[y][x] = region_num
    #
    #             for n in rad_neighbors(x, y):
    #                 if 0 <= n[0] < self.width and 0 <= n[1] < self.height and self.flood_fill_map[n[1]][n[0]] is None:
    #                     stack.append((n[0], n[1]))

            # self.region_map[y][x] = region_num

            # for neigh in lin_neighbors(x, y):
            #     if 0 <= neigh[0] < self.width and 0 <= neigh[1] < self.height and (self.flood_fill_map[neigh[1]][
            #                                                                            neigh[0]] is None or
            #                                                                        self.flood_fill_map[neigh[1]][
            #                                                                            neigh[0]] is orig_fill_type):
            #         stack.append((neigh[0], neigh[1]))

    def flood_tile(self, x, y, region_num):

        stack = [(x,y)]
        orig_fill_type = None

        if self.height_map[y][x] > self.land_threshold:
            orig_fill_type = 1 # Tile is land
        else:
            orig_fill_type = 0 # Tile is water

        while len(stack) > 0:

            x, y = stack.pop()

            if self.region_map[y][x] != 0:
                continue

            if self.height_map[y][x] > self.land_threshold and orig_fill_type == 1:
                self.region_map[y][x] = region_num
                for n in rad_neighbors(x, y):
                    if 0 <= n[0] < self.width and 0 <= n[1] < self.height and self.flood_fill_map[n[1]][n[0]] is None:
                        stack.append((n[0], n[1]))
            elif self.height_map[y][x] < self.land_threshold and orig_fill_type == 0:
                self.region_map[y][x] = region_num
                for n in rad_neighbors(x, y):
                    if 0 <= n[0] < self.width and 0 <= n[1] < self.height and self.flood_fill_map[n[1]][n[0]] is None:
                        stack.append((n[0], n[1]))
            else:
                continue



    def flood_regions(self):

        region_num = 1
        for y in range(0, self.height):
            for x in range(0, self.width):
                if self.flood_fill_map[y][x] is None:  # Discovered new region, start filling it in
                    self.flood_tile(x, y, region_num)
                    region_num += 1
        # print("You have created {} regions".format(region_num))

    def region_colors(self, region_num):

        random.seed(region_num)
        region_color = (random.randint(0, 256), random.randint(0, 256), random.randint(0, 256))

        return region_color

    def draw_region_map(self):

        self.region_image = Image.new("RGB", (self.width, self.height))

        for x in range(0, self.width):
            for y in range(0, self.height):
                self.region_image.putpixel((x, y), self.region_colors(self.region_map[y][x]))

        # plt.imshow(np.asarray(self.region_image))
