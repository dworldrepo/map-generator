# -*- coding: utf-8 -*-
"""
Created on Sat Jul 28 19:25:35 2018

@author: timma_000
"""

import math
import matplotlib.pyplot as plt
import random


def calc_dist(x1, y1, x2, y2):
    """ My own function for calculating distance between points"""
    dx = abs(x1 - x2)
    dy = abs(y1 - y2)
    return math.sqrt(dx ** 2 + dy ** 2)


def norm_dist(x1, y1, x2, y2, map_width, map_height):
    """Used to calculate the normal distance, relative to the size of the map"""
    norm_x = abs(x1 - x2) / map_width
    norm_y = abs(y1 - y2) / map_height
    return math.sqrt(norm_x ** 2 + norm_y ** 2)


def create_histogram(dataPoints, binNum):
    """

    :param dataPoints: List of the data values
    :param binNum: Number of divisions for histogram
    """
    fig, axs = plt.subplots(1, 1)

    axs.hist(dataPoints, bins=binNum)


def lin_neighbors(x, y, radius=1):  # Linear neighbors
    """ Function that returns iterator of all the side touching neighbors

    :param x: xCoord
    :param y: yCoord
    :param radius: How far neighbors should be, 1 by default, so only adjacent neighbors
    :return: Iterator of the linear neighbors, multiple of 4
    """

    for dx in [num for num in range(-radius, radius + 1) if num != 0]:
        yield x + dx, y
    for dy in [num for num in range(-radius, radius + 1) if num != 0]:
        yield x, y + dy


def rad_neighbors(x, y, radius=1):  # Radial neigbors
    """
    Function that returns iterator of all radial neighbors, so including diagonals
    :param x: xCoord
    :param y: yCoord
    :param radius: How far neighbors should be, 1 by default
    :return: Iterator of radial neighbors
    """

    for dx in [num for num in range(-radius, radius + 1)]:
        for dy in [num for num in range(-radius, radius + 1)]:
            if not (dx == 0 and dy == 0):
                yield x + dx, y + dy

def color_regions(region_num):

    random.seed(region_num)
    region_color = (random.randint(0, 256), random.randint(0, 256), random.randint(0, 256))

    return region_color

def most_common(lst):
    return max(set(lst), key=lst.count)

