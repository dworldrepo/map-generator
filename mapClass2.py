from noise import snoise2
import random
from PIL import Image
from pathlib import Path
from copy import deepcopy
import time

from functions import *

# All the big math modules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from mpl_toolkits.mplot3d import Axes3D  # Use to plot 3D planes


class Map:

    def __init__(self, width, height, r_seed, gen_map=True):
        """

        :param width:  Map Width
        :param height: Map Height
        :param r_seed: Seed for repeatable generation of random numbers
        :param gen_map: If true, generates map on init
        """

        self.r_seed_num = r_seed
        random.seed(self.r_seed_num)  # Set the random seed value so we can replicate results

        # General Map Information
        self.m_width = width
        self.m_height = height

        self.shallow_water_threshold = -0.05
        self.land_threshold = 0.0
        self.hill_threshold = 0.3
        self.mountain_threshold = 0.4
        self.snowcap_threshold = 0.53

        self.raise_height = 0.0  # Will raise all height by set amount.

        # Parameters that affect noise generation

        self.octaves = 7  # Affects the level of detail of the noise map. 7 seems nice value
        self.n_frequency = 2.00  # The larger, the more we zoom out of the perlin noise map
        self.cliff_multiplier = 0.95  # Exaggerates cliffs, default is 1.0. Lower = Bigger cliffs

        # Factors that affect regions

        self.micro_region_threshold = 0.0002  # % Threshold. If smaller than x% of map size, will be merged to other region

        # Maps

        self.height_map = np.full((self.m_height, self.m_width), 0.0)
        self.region_map = np.full((self.m_height, self.m_width), 0)

        # Images

        self.height_image = None
        self.region_image = None

        # Other Data Structures

        self.region_dic = {}  # Dictionary of regions on the map

        # Everything Else

        self.h_3D_resolution = 16

        self.time_logs = {}

        # Switch that determines if the map should be generated on intitialization
        self.gen_map = gen_map
        if gen_map:
            self.create_map()

    #############################################################################################################
    # The main method that is used to generate a basic map.
    def create_map(self):
        """
        Fuction that is called to generate the map information. Eventually will take arguments allowing for more
        refined nmap generation.

        :return:
        """
        start = time.time()

        self.gen_b_height_map()
        self.gen_regions_map()

        end = time.time()
        self.time_logs["Create map"] = end - start

    #############################################################################################################
    # The map generators. These simply create all the data, does not visualize or calculate images
    def gen_b_height_map(self):
        """ Generate the basic height map

        :return:
        """

        start = time.time()

        for x in range(self.m_width):
            for y in range(self.m_height):
                self.height_map[y, x] = self.calc_p_height(x, y)

        end = time.time()
        self.time_logs["Generate base height map"] = end - start

    def gen_regions_map(self):
        """
        Base function that determines all the regions in the map
        :return:
        """

        start = time.time()

        self.flood_fill()

        end = time.time()
        self.time_logs["Generate basic region map"] = end - start

    #############################################################################################################
    # Create the images, but only stores, does not visualize. Also helper functions
    def render_height_map(self):
        """
        Function that renders the height map to an image and stores it in self., allowing for easy visualization.

        """
        start = time.time()

        self.height_image = Image.new("RGB", (self.m_width, self.m_height))

        for y in range(self.m_height):
            for x in range(self.m_width):
                self.height_image.putpixel((x, y), self.height_to_color(self.height_map[y, x]))

        end = time.time()
        self.time_logs["Render height map"] = end - start

    def render_3D_h_map(self):
        """
        Render the height map in 3D using matplotlib

        """

        start = time.time()

        x_values = np.arange(0, self.m_width, 1)

        y_values = np.arange(0, self.m_height, 1)

        x_values, y_values = np.meshgrid(x_values, y_values)

        z_values = self.height_map

        fig = plt.figure(figsize=(10, 10), dpi=80, facecolor='w', edgecolor='k')
        ax = fig.add_subplot(1, 1, 1, projection='3d')
        # Because the images are generated from the top left corner, we have to mirror graphs
        plt.gca().invert_yaxis()
        surf = ax.plot_surface(x_values, y_values, z_values, cmap=cm.coolwarm, linewidth=0, antialiased=False)
        ax.view_init(90, 270)  # view_init(elev=None, azim=None) ‘elev’ stores the elevation angle in the z plane.
        # ‘azim’ stores the azimuth angle in the x,y plane.

        # Add a color bar which maps values to colors.
        fig.colorbar(surf, shrink=0.5, aspect=5)

        plt.show()

        end = time.time()
        self.time_logs["Generate 3D height map"] = end - start

    def render_region_map(self):

        start = time.time()

        self.region_image = Image.new("RGB", (self.m_width, self.m_height))
        color_list = [(0, 0, 0)]  # Contains default value because region loop starts at 1, while color_list starts 0

        for region in self.region_dic:
            color_list.append(color_regions(region))

        for x in range(self.m_width):
            for y in range(self.m_height):
                self.region_image.putpixel((x, y), color_list[self.region_map[y, x]])

        end = time.time()
        self.time_logs["Render region map"] = end - start

    def height_to_color(self, height):

        if height > self.snowcap_threshold:
            return 204, 255, 255
        elif height > self.mountain_threshold:
            return 128, 128, 128
        elif height > self.hill_threshold:
            return 134, 89, 45
        elif height > self.land_threshold:
            return 0, 102, 0
        elif height > self.shallow_water_threshold:
            return 51, 173, 255
        else:
            return 51, 102, 255

    #############################################################################################################
    # The basic height generator methods
    def flood_fill(self):
        region_num = 1

        for y in range(self.m_height):
            for x in range(self.m_width):
                if self.region_map[y, x] == 0:
                    self.flood_tile(x, y, region_num)
                    region_num += 1

    # Don't trust PyCharm, simplifying the equations results in errors!
    # noinspection PyChainedComparisons
    def flood_tile(self, x, y, region_num):

        region_size = 0
        stack = [(x, y)]

        orig_fill_type = None

        if self.height_map[y, x] > self.land_threshold:
            orig_fill_type = 1  # Tile is land
        else:
            orig_fill_type = 0  # Tile is water

        while len(stack) > 0:

            x, y = stack.pop()

            if self.region_map[y, x] != 0:
                # Point has already been cleared, move on
                continue

            if self.height_map[y, x] > self.land_threshold and orig_fill_type == 1:
                self.region_map[y, x] = region_num
                region_size += 1
                for n in rad_neighbors(x, y):
                    if 0 <= n[0] < self.m_width and 0 <= n[1] < self.m_height and self.region_map[n[1], n[0]] == 0:
                        stack.append((n[0], n[1]))

            elif self.height_map[y, x] < self.land_threshold and orig_fill_type == 0:
                self.region_map[y, x] = region_num
                region_size += 1
                for n in rad_neighbors(x, y):
                    if 0 <= n[0] < self.m_width and 0 <= n[1] < self.m_height and self.region_map[n[1], n[0]] == 0:
                        stack.append((n[0], n[1]))

            else:
                continue

        self.region_dic[region_num] = {"Region Type": orig_fill_type,
                                       "Region Size": region_size
                                       }

    def calc_p_height(self, x, y):
        """ Calculates individual heights for every single point using noise functions and user parameters

        :param x: xCoord
        :param y: yCoord
        :return: Height of the point, in range 0-1
        """

        # Adding the r_num_seed means we get variety amongst the base height maps

        base_simplex_value = snoise2((x / self.m_width) * self.n_frequency,
                                     (y / self.m_height) * self.n_frequency,
                                     octaves=self.octaves, persistence=0.5, lacunarity=2.0, base=self.r_seed_num)

        # Have to use this because negative numbers raised to powers can have multiple roots, and we don't
        # want complex roots
        final_h_value = np.copysign(np.abs(base_simplex_value) ** self.cliff_multiplier, base_simplex_value) + \
                        self.raise_height

        return final_h_value

    #############################################################################################################
    # More advanced height editing methods
    def h_edit_flatten_ocean(self):
        """
        Functiont that flattens the ocean floor, like it would be in real life.
        :return:
        """
        pass

    #############################################################################################################
    # Other important functions

    def remove_micro_regions(self):

        start = time.time()

        for region in self.region_dic:
            region_rel_size = self.region_dic[region]["Region Size"] / (self.m_height * self.m_width)  # Gives a %

            if region_rel_size < self.micro_region_threshold:
                # Region needs to be removed
                self.remove_region(region)  # Region is simply a number

        end = time.time()
        self.time_logs["Remove micro-regions"] = end - start

    def remove_region(self, region_num):

        # These tiles all have the region_num as neighbours, we go through them later.
        stack = []
        # Check every tile in the region map

        for y in range(self.m_height):
            for x in range(self.m_width):
                if self.region_map[y, x] == region_num:
                    stack.append((x, y))

        while len(stack) > 0:
            x, y = stack.pop(0)  # Take the first from the stack
            neighbour_region_num = []

            for n in lin_neighbors(x, y):
                if 0 <= n[0] < self.m_width and 0 <= n[1] < self.m_height:
                    if self.region_map[n[1], n[0]] == region_num:
                        # Add to the stack, we can't add to neighbour region
                        stack.append((x, y))
                    else:
                        neighbour_region_num.append(self.region_map[n[1], n[0]])

            if len(neighbour_region_num) == 0:
                stack.append((x, y))
            else:
                self.region_map[y, x] = most_common(neighbour_region_num)

    #############################################################################################################
    # Save images and data

    def save_map_data(self, save_data=False, save_other_images=False):

        start = time.time()

        # The map number is based on the height map, which is always generated
        map_num = 1

        height_image_path = Path("images/heightMap{}.png".format(map_num))

        while height_image_path.is_file():
            map_num += 1
            height_image_path = Path("images/heightMap{}.png".format(map_num))
        else:
            self.height_image.save("images/heightMap{}.png".format(map_num))

        if save_other_images:
            self.region_image.save("images/regionMap{}.png".format(map_num))

        if save_data:
            self.save_data_logs(map_num)

        end = time.time()
        self.time_logs["Generate all data and images"] = end - start

    def save_data_logs(self, map_num):

        start = time.time()

        with open("data/map{}.txt".format(map_num), "w", newline=None) as txtfile:
            do_not_add = ["height_map", "region_map"]
            for attribute in self.__dict__:
                if attribute not in do_not_add:

                    if type(self.__dict__[attribute]) == dict:  # Make dict more readable in file
                        txtfile.write(attribute + ":\n")
                        for thing in self.__dict__[attribute]:
                            txtfile.write("\t" + str(thing) + ": " + str(self.__dict__[attribute][thing]) + "\n")
                        txtfile.write("\n")

                    else:
                        line_text = attribute + ": " + str(self.__dict__[attribute]) + "\n\n"
                        txtfile.write(line_text)

        end = time.time()
        self.time_logs["Generate only data logs"] = end - start
